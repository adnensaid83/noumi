export interface SocialI {
  id: string;
  title: string;
  url: string;
}

export interface CoverImageI {
  responsiveImage: {
    srcSet: string;
    webpSrcSet: string;
    sizes: string;
    src: string;
    width: number;
    height: number;
    aspectRatio: number;
    alt: string;
    title: string;
    base64: string;
  };
}
