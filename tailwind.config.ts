import type { Config } from "tailwindcss";
const { fontFamily } = require("tailwindcss/defaultTheme");
const config: Config = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      colors: {
        primary: "",
        secondary: "",
        black: "#333333",
        black100: "rgba(0,0,0,.8)",
        black200: "rgba(0,0,0,.6)",
        white100: "rgba(255,255,255,.8)",
        black50: "rgba(0,0,0,.2)",
        tertiary: {
          dark: "#F27405",
          light: "#d9af28",
        },
      },
      fontFamily: {
        poppins: ["var(--font-poppins)", ...fontFamily.sans],
      },
      backgroundImage: {
        bannerImage1: "url('/banner.png')",
      },
    },
  },
  plugins: [],
};
export default config;
