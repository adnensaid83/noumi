"use client";
import { useEffect, useRef } from "react";
import { register } from "swiper/element/bundle";
import { russoOne } from "@/app/ui/fonts";
register();
const BannerSwiper = () => {
  const swiperRef = useRef(null);
  useEffect(() => {
    const swiperContainer = swiperRef.current;
    const params = {
      navigation: true,
      pagination: true,
      injectStyles: [
        `
          .swiper-button-next,
          .swiper-button-prev {
            display:none;
          }
          .swiper-button-prev {
            background-image: url('./arrow.png');
            background-position: center;
            background-size: contain;
            transform:rotate(180deg);
            margin-left:8vw;
          }

          .swiper-button-next {
            background-image: url("./arrow.png");
            background-position: center;
            background-size: contain;
            margin-right:8vw;
          }
          .swiper-button-next::after,
          .swiper-button-prev::after {
            content: "";
          }
          .swiper-pagination-bullet{
            width: 16px;
            height: 16px;
            border:2px solid white;
            transform:translateY(-48px);
          }
          .swiper-pagination-bullet-active{
            background-color: #d9af28;
          }
          @media (min-width: 768px) {
            .swiper-button-next,
            .swiper-button-prev {
              display:block;
              padding: 8px 16px;
              color: transparent;
            }
            .swiper-pagination-bullet{
              width: 20px;
              height: 20px;
              transform:translateY(-60px);
            }
          }
      `,
      ],
    };

    Object.assign(swiperContainer, params);
    swiperContainer.initialize();
  }, []);
  return (
    <swiper-container ref={swiperRef} init="false">
      <swiper-slide class="h-[50vh] lg:h-[calc(100vh-72px)] flex items-center justify-center bg-bannerImage1 bg-no-repeat bg-cover bg-center brightness-75">
        <SlideOne />
      </swiper-slide>
      <swiper-slide class="h-[50vh] lg:h-[calc(100vh-72px)] flex items-center justify-center bg-bannerImage1 bg-no-repeat bg-cover bg-center brightness-75">
        slide2
      </swiper-slide>
      <swiper-slide class="h-[50vh] lg:h-[calc(100vh-72px)] flex items-center justify-center bg-bannerImage1 bg-no-repeat bg-cover bg-center brightness-75">
        slide3
      </swiper-slide>
    </swiper-container>
  );
};

export default BannerSwiper;

function SlideOne() {
  return (
    <div className={` ${russoOne.className}`}>
      <h1 className="text-center text-white100 mb-8 text-5xl lg:text-[120px] lg:mb-28">
        NOUMICAR
      </h1>
      <p
        className={`uppercase text-center text-white text-4xl leading-normal lg:text-[70px]`}
      >
        Votre vehicule sur <span className="text-tertiary-light">mesure</span>
      </p>
    </div>
  );
}
