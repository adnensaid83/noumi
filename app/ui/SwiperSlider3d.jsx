"use client";
import { russoOne } from "@/fonts/fonts";
import Image from "next/image";
import React, { useEffect, useRef } from "react";

export const SwiperSlider3d = ({ data }) => {
  const swiperRef = useRef(null);

  useEffect(() => {
    const swiperContainer = swiperRef.current;
    const params = {
      navigation: true,
      pagination: true,
      effectCoverflow: true,
      effect: "coverflow",
      grabCursor: true,
      centeredSlides: true,
      loop: true,
      coverflowEffect: {
        rotate: 0,
        stretch: 0,
        depth: 100,
        modifier: 2.5,
      },
      slidesPerView: 2,
      injectStyles: [
        `
            .swiper-button-next,
            .swiper-button-prev {
                display:none;
              }
              .swiper-button-prev {
                background-image: url('./arrow.png');
                background-repeat:no-repeat;
                background-position: center;
                background-size: contain;
                transform:rotate(180deg);
                width:5px;
              }
    
              .swiper-button-next {
                background-image: url("./arrow.png");
                background-position: center;
                background-size: contain;
                background-repeat:no-repeat;
                background-position: center;
                width:5px;
              }
              .swiper-button-next::after,
              .swiper-button-prev::after {
                content: "";
              }
    
              .swiper-pagination-bullet{
                width: 10px;
                height: 10px;
                border:2px solid white;
                transform:translateY(-10%);
                
              }
              .swiper-pagination-bullet-active{
                background-color: #d9af28;
              }
              @media (min-width: 768px) {
                .swiper-button-next,
                .swiper-button-prev {
                  display:block;
                  padding: 8px 16px;
                  color: transparent;
                }
              }
          `,
      ],
    };

    Object.assign(swiperContainer, params);
    swiperContainer.initialize();
  }, []);
  return (
    <swiper-container ref={swiperRef} init="false" class="px-16">
      {data.map((d, i) => {
        return (
          <swiper-slide key={d.id}>
            <div className="relative">
              <Image
                src={d.coverImage.src}
                alt={d.coverImage.alt}
                width={590}
                height={590}
                className=" w-full w-84"
                priority
              />
              <div className="flex flex-col items-center  absolute w-full top-0">
                <div className="bg-black200 text-white w-full p-2 md:p-4">
                  <p
                    className={`text-xl md:text-2xl text-center uppercase ${russoOne.className}`}
                  >
                    {d.title}
                  </p>
                  <p className={`md:text-xl font-light text-center`}>
                    {d.price}
                  </p>
                </div>
              </div>
            </div>
          </swiper-slide>
        );
      })}
      {/*       <swiper-slide>
        <div className="relative">
          <Image
            src="/jeep.png"
            alt="jeep compass"
            width={590}
            height={590}
            className=" w-full w-84"
            priority
          />
          <div className="flex flex-col items-center  absolute w-full top-0">
            <div className="bg-black200 text-white w-full p-2 md:p-4">
              <p
                className={`text-xl md:text-2xl text-center uppercase ${russoOne.className}`}
              >
                lorem ipsum
              </p>
              <p className={`md:text-xl font-light text-center`}> 23 500 € </p>
            </div>
          </div>
        </div>
      </swiper-slide>
      <swiper-slide>
        <div className="relative">
          <Image
            src="/fordfiesta.png"
            alt="ford fiesta"
            width={590}
            height={590}
            className=" w-full w-84"
            priority
          />
        </div>
      </swiper-slide>
      <swiper-slide>
        <div className="relative">
          <Image
            src="/jeep.png"
            alt="jeep compass"
            width={590}
            height={590}
            className=" w-full w-84"
            priority
          />
        </div>
      </swiper-slide>
      <swiper-slide>
        <div className="relative">
          <Image
            src="/jeep.png"
            alt="jeep compass"
            width={590}
            height={590}
            className=" w-full w-84"
            priority
          />
        </div>
      </swiper-slide>
      <swiper-slide>
        <div className="relative">
          <Image
            src="/mercedesblue.png"
            alt="mercedes class c"
            className=" w-full w-84"
            width={590}
            height={590}
            priority
          />
          <div></div>
        </div>
      </swiper-slide> */}
    </swiper-container>
  );
};
