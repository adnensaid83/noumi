import Link from "next/link";
import NavLinks from "./nav-links";
import SocialLinks from "../socials/social-links";
import X from "./X";
import { NavbarToggler } from "./NavbarToggler";
import { russoOne } from "../fonts";

const CollapseNavbarMenu = ({
  refObj,
  onCollapse,
}: {
  refObj: any;
  onCollapse: () => void;
}) => {
  return (
    <div className="hidden lg:hidden" ref={refObj}>
      <div
        className={`fixed min-h-screen inset-y-0 right-0 left-0 top-0 bottom-0 w-full flex flex-col overflow-y-auto bg-white`}
      >
        <div className="flex justify-between items-center py-4 px-6 lg:py-6">
          <Link
            href={"/"}
            className={`font-bold text-tertiary-light uppercase text-2xl ${russoOne.className}`}
          >
            Noumi<span className="text-black">car</span>
          </Link>
          <div className="flex lg:hidden">
            <NavbarToggler
              imgCompnent={<X />}
              onCollapse={onCollapse}
              className={` w-[36px] h-[36px] p-0 text-black focus:outline-0 `}
            />
          </div>
        </div>
        <div className="flex-1 flex flex-col justify-center gap-12">
          <ul className="list-none text-center font-light text-xl space-y-0">
            <NavLinks onCollapse={onCollapse} />
          </ul>
          <ul className="flex gap-x-5 list-none justify-center">
            <SocialLinks />
          </ul>
        </div>
      </div>
    </div>
  );
};

export default CollapseNavbarMenu;
