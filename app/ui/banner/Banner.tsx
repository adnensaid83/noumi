import React from "react";
import BannerSwiper from "../BannerSwiper/BannerSwiper";

export const Banner = () => {
  return (
    <div className="relative">
      <BannerSwiper />
    </div>
  );
};
