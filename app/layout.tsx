import type { Metadata } from "next";
import "@/app/ui/global.css";
import Footer from "./ui/footer/Footer";
import Navbar from "./ui/header/Navbar";
import { poppins } from "./ui/fonts";

export const metadata: Metadata = {
  title: "Noumi car App",
  description: "Vente de voitures d'occasion",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={`${poppins.className} antialiased`}>
        <main className="font-normal flex min-h-screen flex-col">
          <Navbar />
          {children}
          <Footer />
        </main>
      </body>
    </html>
  );
}
