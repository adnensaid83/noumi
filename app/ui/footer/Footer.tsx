import { russoOne } from "@/app/ui/fonts";
import Image from "next/image";
import Link from "next/link";
import React from "react";
import { SlArrowRight } from "react-icons/sl";

const Footer = () => {
  return (
    <div className="relative mt-20">
      <iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2782.2299421167686!2d4.792234276231732!3d45.786620871081006!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47f4eb5e641b7e75%3A0xbfad91525864e124!2s2%20Av.%20Rosa%20Parks%2C%2069009%20Lyon!5e0!3m2!1sfr!2sfr!4v1704585407409!5m2!1sfr!2sfr"
        loading="lazy"
        width={"100%"}
        className="h-[950px] md:h-[500px]"
      ></iframe>
      <div className="absolute w-full h-full top-0 px-6 bg-black100">
        <div className="bg-tertiary-light text-white py-4 px-4 max-w-5xl -translate-y-20 md:-translate-y-24 lg:-translate-y-20 md:py-6 md:px-12 lg:mx-auto">
          <h3 className={`uppercase text-2xl mb-4 ${russoOne.className}`}>
            À propos
          </h3>
          <p className="lg:text-xl">
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptatum
            saepe corporis officia illo placeat iusto quas, eveniet molestiae.
            Reprehenderit, ea quaerat! Placeat ipsam, dicta vel molestiae hic
            excepturi consequuntur? Quos eveniet molestiae.
          </p>
        </div>
        <div className=" md:max-w-4xl md:gap-12 md:mx-6 md:flex md:justify-between lg:mx-auto">
          <div className="text-white mb-12 md:w-1/3">
            <Link
              href={"/"}
              className={`font-bold text-tertiary-light uppercase text-4xl block mb-4 ${russoOne.className}`}
            >
              Noumi<span className="text-white">car</span>
            </Link>
            <p className="text-sm">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas
              deserunt, eaque numquam beatae asperiores dicta non nulla cum
              dolor, suscipit cumque eveniet at ipsa eum quae, facilis earum eos
              voluptatibus?
            </p>
          </div>
          <div className="text-white mb-12 w-auto">
            <h3 className="text-xl font-medium mb-4 uppercase">
              Informations de contact
            </h3>
            <ul className="flex flex-col gap-4">
              <li className="flex items-center gap-4">
                <Image
                  src="/mapIcon.png"
                  alt="map"
                  className="light:invert w-5 h-5"
                  width={40}
                  height={40}
                  priority
                />
                <p className="text-sm">
                  2 avenue rosa parks 69009 Lyon, France
                </p>
              </li>
              <li className="flex items-center gap-4">
                <Image
                  src="/phoneIcon.png"
                  alt="phone"
                  className="light:invert w-5 h-5"
                  width={40}
                  height={40}
                  priority
                />
                <p className="text-sm">+33 78976543245</p>
              </li>
              <li className="flex items-center gap-4">
                <Image
                  src="/emailIcon.png"
                  alt="email"
                  className="light:invert w-5 h-5"
                  width={40}
                  height={40}
                  priority
                />
                <p className="text-sm">loripsum@gmail.com</p>
              </li>
            </ul>
          </div>
          <div className=" w-[150px] text-white">
            <h3 className="text-xl font-medium mb-4 uppercase">Nos packs</h3>
            <ul className="flex flex-col gap-4">
              <li className="flex items-center gap-4">
                <SlArrowRight className="w-3 text-tertiary-light" />
                <p className="text-sm">Lorem ipsum</p>
              </li>
              <li className="flex items-center gap-4">
                <SlArrowRight className="w-3 text-tertiary-light" />
                <p className="text-sm">Lorem ipsum</p>
              </li>
              <li className="flex items-center gap-4">
                <SlArrowRight className="w-3 text-tertiary-light" />
                <p className="text-sm">Lorem ipsum</p>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
