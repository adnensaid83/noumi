"use client";
import React from "react";
import { Rating, ThinStar } from "@smastrom/react-rating";
import "@smastrom/react-rating/style.css";
import Swiper3d from "../Swiper3d";
import Image from "next/image";
import { russoOne } from "@/app/ui/fonts";

const myStyles = {
  itemShapes: ThinStar,
  activeFillColor: "#d9af28",
  inactiveFillColor: "#fbf1a9",
};
type TestimonialsProps = {
  id: string;
  fullname: string;
  message: string;
  personImage: {
    src: string;
    alt: string;
  };
  coverImage: {
    src: string;
    alt: string;
  };
  rating: number;
};

export const Testimonials = ({
  testimonials,
}: {
  testimonials: TestimonialsProps[];
}) => {
  const params = {
    navigation: true,
    pagination: true,
    effectCoverflow: true,
    effect: "coverflow",
    grabCursor: true,
    centeredSlides: true,
    loop: true,
    coverflowEffect: {
      rotate: 0,
      stretch: 0,
      depth: 100,
      modifier: 2.5,
    },
    breakpoints: {
      640: {
        slidesPerView: 2,
      },
      768: {
        slidesPerView: 2.5,
      },
      1024: {
        slidesPerView: 3.5,
      },
    },
    injectStyles: [
      `
              .swiper-button-next,
              .swiper-button-prev {
                  display:none;
                }
                .swiper-button-prev {
                  background-image: url('./arrow.png');
                  background-repeat:no-repeat;
                  background-position: center;
                  background-size: contain;
                  transform:rotate(180deg);
                  width:5px;
                }
      
                .swiper-button-next {
                  background-image: url("./arrow.png");
                  background-position: center;
                  background-size: contain;
                  background-repeat:no-repeat;
                  background-position: center;
                  width:5px;
                }
                .swiper-button-next::after,
                .swiper-button-prev::after {
                  content: "";
                }
                .swiper-pagination{
                  transform: translateY(10px);
                  background:#333;
                }
                .swiper-pagination-bullet{
                  width: 10px;
                  height: 10px;
                  border:2px solid white;    
                }
                .swiper-pagination-bullet-active{
                  background-color: #d9af28;
                }
                @media (min-width: 768px) {
                  .swiper-button-next,
                  .swiper-button-prev {
                    display:block;
                    padding: 8px 16px;
                    color: transparent;
                  }
                  .swiper-pagination{
                    background:#fff;
                  }
                }
            `,
    ],
  };
  return (
    <section className="py-12 px-6 bg-white lg:px-0">
      <h2
        className={`uppercase text-3xl md:text-4xl text-center mb-12 text-black ${russoOne.className}`}
      >
        Nos <span className="text-tertiary-light">clients</span>
      </h2>
      <div className="container mx-auto">
        <Swiper3d
          items={testimonials}
          resourceName="testimonial"
          itemComponent={TestimonialsItem}
          params={params}
        />
      </div>
    </section>
  );
};

function TestimonialsItem({ testimonial }: { testimonial: TestimonialsProps }) {
  const { id, personImage, fullname, message, coverImage, rating } =
    testimonial;
  return (
    <div>
      <div className="relative">
        <Image
          src={coverImage.src}
          alt={coverImage.alt}
          width={590}
          height={590}
          className="w-full"
          priority
        />
        <div className="flex flex-row items-center justify-center gap-2 py-2 absolute bottom-0 w-full bg-black200">
          <Rating
            style={{ maxWidth: 140 }}
            value={rating}
            itemStyles={myStyles}
          />
          <Image
            src={personImage.src}
            alt={personImage.alt}
            width={60}
            height={60}
            className=" h-14 w-14 rounded-full"
            priority
          />
        </div>
      </div>
      <div className="px-12 py-4 bg-black text-white">
        <p className="text-center mb-4 text-tertiary-light"> {fullname} </p>
        <p className="text-base text-center pb-4 lg:pb-6"> {message} </p>
      </div>
    </div>
  );
}
