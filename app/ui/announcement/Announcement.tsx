"use client";
import React from "react";

import "swiper/css";
import "swiper/css/effect-coverflow";
import "swiper/css/pagination";
import "swiper/css/navigation";
import Image from "next/image";
import Swiper3d from "../Swiper3d";
import Link from "next/link";
import { russoOne } from "@/app/ui/fonts";

type AnnouncementProps = {
  id?: string;
  title: string;
  price: string;
  coverImage: {
    src: string;
    alt: string;
  };
};
export const Announcement = ({ ads }: { ads: AnnouncementProps[] }) => {
  const params = {
    navigation: true,
    pagination: true,
    effectCoverflow: true,
    effect: "coverflow",
    grabCursor: true,
    centeredSlides: true,
    loop: true,
    coverflowEffect: {
      rotate: 0,
      stretch: 0,
      depth: 100,
      modifier: 2.5,
    },
    breakpoints: {
      640: {
        slidesPerView: 1,
      },
      768: {
        slidesPerView: 2,
      },
    },
    injectStyles: [
      `
              .swiper-button-next,
              .swiper-button-prev {
                  display:none;
                }
                .swiper-button-prev {
                  background-image: url('./arrow.png');
                  background-repeat:no-repeat;
                  background-position: center;
                  background-size: contain;
                  transform:rotate(180deg);
                  width:5px;
                }
      
                .swiper-button-next {
                  background-image: url("./arrow.png");
                  background-position: center;
                  background-size: contain;
                  background-repeat:no-repeat;
                  background-position: center;
                  width:5px;
                }
                .swiper-button-next::after,
                .swiper-button-prev::after {
                  content: "";
                }
                .swiper-pagination-bullet{
                  width: 10px;
                  height: 10px;
                  border:2px solid white;
                  transform:translateY(-10%);
                  
                }
                .swiper-pagination-bullet-active{
                  background-color: #d9af28;
                }
                @media (min-width: 768px) {
                  .swiper-button-next,
                  .swiper-button-prev {
                    display:block;
                    padding: 8px 16px;
                    color: transparent;
                  }
                }
            `,
    ],
  };
  return (
    <section className="pb-12 px-6 bg-white lg:px-0">
      <span className="block w-36 h-[4px] bg-tertiary-light mx-auto mb-12 lg:h-2"></span>
      <h2
        className={`uppercase text-3xl md:text-4xl text-center mb-12 text-black ${russoOne.className}`}
      >
        Nos <span className="text-tertiary-light">annonces</span>
      </h2>
      <div className="container mx-auto mb-8">
        <Swiper3d
          items={ads}
          resourceName="ads"
          itemComponent={AnnouncementItem}
          params={params}
        />
      </div>
      <div className="flex justify-center">
        <Link
          href={"/"}
          className="border-2 border-transparent text-white bg-tertiary-light text-2xl px-12 rounded-xl text-center transition ease-in-out duration-300 hover:bg-transparent hover:text-tertiary-light hover:border-tertiary-light"
        >
          Détails
        </Link>
      </div>
    </section>
  );
};

function AnnouncementItem({ ads }: { ads: AnnouncementProps }) {
  const { title, price, coverImage } = ads;
  return (
    <div className="relative">
      <Image
        src={coverImage.src}
        alt={coverImage.alt}
        width={590}
        height={590}
        className=" w-full w-84"
        priority
      />
      <div className="flex flex-col items-center  absolute w-full top-0">
        <div className="bg-black200 text-white w-full p-2 md:p-4">
          <p
            className={`text-xl md:text-2xl text-center uppercase ${russoOne.className}`}
          >
            {title}
          </p>
          <p className={`md:text-xl font-light text-center`}>{price}</p>
        </div>
      </div>
    </div>
  );
}
