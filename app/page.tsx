import Image from "next/image";
import { Banner } from "@/app/ui/banner/Banner";
import { NoPacks } from "./ui/noPacks/NoPacks";
import { NosValeurs } from "./ui/nosValeurs/NosValeurs";
import { Announcement } from "./ui/announcement/Announcement";
import { HowItWorks } from "./ui/howitworks/HowItWorks";
import { Testimonials } from "./ui/testimonials/Testimonials";

const packs = [
  {
    id: "1",
    title: "lorem ipsum",
    price: "1000€",
    coverImage: {
      src: "/polo.png",
      alt: "polo",
    },
  },
  {
    id: "2",
    title: "lorem ipsum",
    price: "1500€",
    coverImage: {
      src: "/audi.png",
      alt: "audi",
    },
  },
  {
    id: "3",
    title: "lorem ipsum",
    price: "2000€",
    coverImage: {
      src: "/quatquat.png",
      alt: "range rover",
    },
  },
  {
    id: "4",
    title: "lorem ipsum",
    price: "3000€",
    coverImage: {
      src: "/bentely.png",
      alt: "bentely",
    },
  },
];
const valeurs = [
  {
    id: "1",
    title: "Prix | qualité",
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Deserunt expedita alias enim, laudantium modi, accusantium incidunt libero illo, soluta sit dolorum.",
    coverImage: {
      src: "/prix_qualite.png",
      alt: "prix qualité",
    },
  },
  {
    id: "2",
    title: "Securité",
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Deserunt expedita alias enim, laudantium modi, accusantium incidunt libero illo, soluta sit dolorum.",
    coverImage: {
      src: "/securite.png",
      alt: "securité",
    },
  },
  {
    id: "3",
    title: "Fiabilité",
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Deserunt expedita alias enim, laudantium modi, accusantium incidunt libero illo, soluta sit dolorum.",
    coverImage: {
      src: "/fiabilite.png",
      alt: "fiabilite",
    },
  },
  {
    id: "4",
    title: "Rapidité",
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Deserunt expedita alias enim, laudantium modi, accusantium incidunt libero illo, soluta sit dolorum.",
    coverImage: {
      src: "/rapidite.png",
      alt: "rapidite",
    },
  },
];
const ads = [
  {
    id: "1",
    title: "Jeep Compass (2020)",
    price: "23 500 €",
    coverImage: {
      src: "/jeep.png",
      alt: "jeep compass",
    },
  },
  {
    id: "2",
    title: "Ford Fiesta",
    price: "12 000 €",
    coverImage: {
      src: "/fordfiesta.png",
      alt: "ford fiesta",
    },
  },
  {
    id: "3",
    title: "Mercedes Class C",
    price: "18 000 €",
    coverImage: {
      src: "/mercedesblue.png",
      alt: "mercedes class c",
    },
  },
  {
    id: "4",
    title: "Ford Fiesta",
    price: "12 000 €",
    coverImage: {
      src: "/fordfiesta.png",
      alt: "ford fiesta",
    },
  },
];
const howitworks = [
  {
    id: "1",
    title: "Entretien téléphonique",
    coverImage: {
      src: "/cma1.png",
      alt: "comment ça marche",
    },
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae laudantium, ullam necessitatibus dignissimos iste ut.",
    path: "",
  },
  {
    id: "2",
    title: "Recherche véhicules",
    coverImage: {
      src: "/cma2.png",
      alt: "Recherche véhicules",
    },
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae laudantium, ullam necessitatibus dignissimos iste ut.",
    path: "",
  },
  {
    id: "3",
    title: "Contrôle",
    coverImage: {
      src: "/cma3.png",
      alt: "Contrôle",
    },
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae laudantium, ullam necessitatibus dignissimos iste ut.",
    path: "",
  },
  {
    id: "4",
    title: "Achat",
    coverImage: {
      src: "/cma4.png",
      alt: "achat",
    },
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae laudantium, ullam necessitatibus dignissimos iste ut.",
    path: "",
  },
  {
    id: "5",
    title: "Livraison",
    coverImage: {
      src: "/cma5.png",
      alt: "livraison",
    },
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae laudantium, ullam necessitatibus dignissimos iste ut.",
    path: "",
  },
];

const testimonials = [
  {
    id: "1",
    fullname: "Adnen Said",
    message:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae laudantium, ullam necessitatibus dignissimos iste ut.",
    personImage: {
      src: "/adnen.jpg",
      alt: "adnen",
    },
    coverImage: {
      src: "/testimonials1.png",
      alt: "polo",
    },
    rating: 4,
  },
  {
    id: "2",
    fullname: "Jonah Chrofit",
    message:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae laudantium, ullam necessitatibus dignissimos iste ut.",
    personImage: {
      src: "/chrofit.jpg",
      alt: "chrofit",
    },
    coverImage: {
      src: "/testimonials2.png",
      alt: "golf",
    },
    rating: 4,
  },
  {
    id: "3",
    fullname: "Andrea Pierot",
    message:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae laudantium, ullam necessitatibus dignissimos iste ut.",
    personImage: {
      src: "/andrea.jpg",
      alt: "andrea",
    },
    coverImage: {
      src: "/testimonials3.png",
      alt: "Golf",
    },
    rating: 4,
  },
  {
    id: "4",
    fullname: "Andrea Pierot",
    message:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae laudantium, ullam necessitatibus dignissimos iste ut.",
    personImage: {
      src: "/andrea.jpg",
      alt: "andrea",
    },
    coverImage: {
      src: "/testimonials3.png",
      alt: "Golf",
    },
    rating: 4,
  },
  {
    id: "5",
    fullname: "Andrea Pierot",
    message:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae laudantium, ullam necessitatibus dignissimos iste ut.",
    personImage: {
      src: "/andrea.jpg",
      alt: "andrea",
    },
    coverImage: {
      src: "/testimonials3.png",
      alt: "Golf",
    },
    rating: 4,
  },
  {
    id: "6",
    fullname: "Andrea Pierot",
    message:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae laudantium, ullam necessitatibus dignissimos iste ut.",
    personImage: {
      src: "/andrea.jpg",
      alt: "andrea",
    },
    coverImage: {
      src: "/testimonials3.png",
      alt: "Golf",
    },
    rating: 4,
  },
  {
    id: "7",
    fullname: "Andrea Pierot",
    message:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae laudantium, ullam necessitatibus dignissimos iste ut.",
    personImage: {
      src: "/andrea.jpg",
      alt: "andrea",
    },
    coverImage: {
      src: "/testimonials3.png",
      alt: "Golf",
    },
    rating: 4,
  },
  {
    id: "8",
    fullname: "Jonah Chrofit",
    message:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae laudantium, ullam necessitatibus dignissimos iste ut.",
    personImage: {
      src: "/chrofit.jpg",
      alt: "chrofit",
    },
    coverImage: {
      src: "/testimonials2.png",
      alt: "golf",
    },
    rating: 4,
  },
  {
    id: "9",
    fullname: "Jonah Chrofit",
    message:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae laudantium, ullam necessitatibus dignissimos iste ut.",
    personImage: {
      src: "/chrofit.jpg",
      alt: "chrofit",
    },
    coverImage: {
      src: "/testimonials2.png",
      alt: "golf",
    },
    rating: 4,
  },
  {
    id: "10",
    fullname: "Jonah Chrofit",
    message:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae laudantium, ullam necessitatibus dignissimos iste ut.",
    personImage: {
      src: "/chrofit.jpg",
      alt: "chrofit",
    },
    coverImage: {
      src: "/testimonials2.png",
      alt: "golf",
    },
    rating: 4,
  },
];
export default function Home() {
  return (
    <div className="min-h-screen">
      <Banner />
      <NoPacks packs={packs} />
      <NosValeurs valeurs={valeurs} />
      <Announcement ads={ads} />
      <HowItWorks data={howitworks} />
      <Testimonials testimonials={testimonials} />
    </div>
  );
}
