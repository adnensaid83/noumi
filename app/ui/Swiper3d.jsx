import React, { use, useEffect, useRef } from "react";
const Swiper3d = ({
  items,
  resourceName,
  itemComponent: ItemComponent,
  params,
}) => {
  const swiperRef = useRef(null);
  useEffect(() => {
    const swiperContainer = swiperRef.current;
    Object.assign(swiperContainer, params);
    swiperContainer.initialize();
  }, []);
  return (
    <swiper-container ref={swiperRef} init="false" class="px-0 lg:px-16">
      {items.map((item, index) => (
        <swiper-slide key={index}>
          <ItemComponent {...{ [resourceName]: item }} />
        </swiper-slide>
      ))}
    </swiper-container>
  );
};

export default Swiper3d;
