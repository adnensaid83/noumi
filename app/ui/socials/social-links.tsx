import Link from "next/link";
import React from "react";
import Instagram from "./Instagrem";

const links = [{ name: "instagram", href: "", icon: <Instagram /> }];
const SocialLinks = () => {
  return (
    <>
      {links.map((link) => {
        return (
          <Link
            key={link.name}
            href={link.href}
            className={
              "w-6 flex items-center justify-center border-dark text-black md:hover:opacity-80 md:hover:text-tertiary-light"
            }
          >
            {link.icon}
          </Link>
        );
      })}
    </>
  );
};

export default SocialLinks;
