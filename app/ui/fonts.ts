import { Racing_Sans_One, Russo_One, Poppins } from "next/font/google";

export const racingSansOne = Racing_Sans_One({
  subsets: ["latin"],
  weight: ["400"],
  style: ["normal"],
  variable: "--font-racing-sans-one",
});

export const russoOne = Russo_One({
  weight: "400",
  subsets: ["latin"],
  style: ["normal"],
});

export const poppins = Poppins({
  subsets: ["latin"],
  weight: ["400", "500", "700", "900"],
  style: ["italic", "normal"],
  variable: "--font-poppins",
});
