# Projet de Vente de Voitures d'Occasion - NoumiCar

![maquette](public/maquettes/noumi.png)

# Description

Bienvenue chez NoumiCar, votre destination incontournable pour l'achat de voitures d'occasion. La plateforme révolutionne l'expérience d'achat en proposant une gamme complète de services personnalisés, adaptés aux besoins de chaque client.

# Languages utilisés

- NextJS, Typescript, Formiz
- Tailwindcss, SwiperJS

# Responsive

Oui, il est responsive

# Aperçu du Projet

NoumiCar se distingue en offrant non seulement une sélection de voitures d'occasion de qualité, mais également en simplifiant le processus d'achat grâce à des packs de services exclusifs. Nous comprenons que chaque client a des besoins uniques, c'est pourquoi nous avons créé quatre packs distincts pour répondre à toutes les attentes.

# Les Packs NoumiCar

1. **Pack Basic - "Explore":** Vous êtes à la recherche de la voiture parfaite en France ou à travers l'Europe ? Notre Pack Basic vous permet de chercher et de trouver la voiture idéale, avec une couverture étendue pour répondre à vos besoins de mobilité.
2. **Pack Advanced - "Drive & Collect":** Pour une expérience sans tracas, optez pour notre Pack Advanced. Nous trouvons la voiture selon vos critères, nous allons la chercher, et vous la récupérez directement dans notre garage. Profitez de la facilité d'une acquisition sans souci.
3. **Pack Premium - "Luxury Care":** Pour ceux qui recherchent l'excellence, notre Pack Premium inclut toutes les fonctionnalités du Pack Advanced, avec en plus un lavage complet de la voiture et la gestion de toutes les démarches administratives. Profitez de votre nouvelle voiture avec style et tranquillité d'esprit.
4. **Pack VIP - "Tailored Perfection":**Pour une expérience sur mesure, contactez-nous pour discuter de vos besoins spécifiques. Notre équipe dévouée travaillera avec vous pour créer une solution personnalisée répondant à toutes vos exigences.

# UI

![step1](public/maquettes/dashboard-1.png)

![step2](public/maquettes/dashboard-2.png)

![step3](public/maquettes/dashboard-3.png)

![step4](public/maquettes/login.png)

![step5](public/maquettes/order-1.png)

![step6](public/maquettes/order-2.png)

![step7](public/maquettes/order-3.png)

![step78](public/maquettes/order-4.png)
