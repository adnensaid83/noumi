"use client";
import Link from "next/link";
import { usePathname } from "next/navigation";
import clsx from "clsx";

const links = [
  {
    name: "Acceuil",
    href: "/",
  },
  {
    name: "Nos packs",
    href: "",
  },
  {
    name: "Nos annonces",
    href: "/announcement",
  },
  {
    name: "Contact",
    href: "/contact",
  },
];
const NavLinks = ({ onCollapse }: { onCollapse: () => void }) => {
  const pathname = usePathname();

  return (
    <>
      {links.map((link) => {
        return (
          <Link
            key={link.name}
            href={link.href}
            className={clsx(
              "block mt-0 font-medium text-2xl text-black hover:opacity-80 py-6 lg:text-xl lg:px-6 lg:py-3",
              {
                "text-tertiary-light": pathname === link.href,
              }
            )}
            onClick={() => onCollapse()}
          >
            <p className="block">{link.name}</p>
          </Link>
        );
      })}
    </>
  );
};

export default NavLinks;
