"use client";
import List from "./List";
import Link from "next/link";
import React, { ReactNode, useRef } from "react";
import X from "./X";
import { russoOne } from "@/app/ui/fonts";
import NavLinks from "./nav-links";
import SocialLinks from "../socials/social-links";
import { NavbarToggler } from "./NavbarToggler";
import CollapseNavbarMenu from "./CollapseNavbarMenu";

const Navbar = () => {
  const collapseNavbarMenuRef = useRef<HTMLDivElement | null>(null);

  const handleCollapseNavbarMenu = () => {
    const collapse = collapseNavbarMenuRef.current;
    collapse?.classList.toggle("hidden");
  };
  return (
    <header className="px-6 sticky top-0 shadow-nav backdrop-blur z-40 bg-[rgba(255,255,255,.6)]">
      <div className="container mx-auto">
        <nav className="flex items-center justify-between py-4 md:gap-20 lg:justify-start">
          <Link
            href={"/"}
            className={`font-bold text-tertiary-light uppercase text-2xl ${russoOne.className}`}
          >
            Noumi<span className="text-black">car</span>
          </Link>
          <div className="flex lg:hidden">
            <NavbarToggler
              imgCompnent={<List />}
              onCollapse={handleCollapseNavbarMenu}
              className={` w-[36px] h-[36px] text-black p-0 focus:outline-0 `}
            />
          </div>
          <div className="hidden lg:flex-1 lg:flex">
            <div className="relative flex-1 flex items-center justify-between gap-6 ">
              <ul className="flex">
                <NavLinks onCollapse={handleCollapseNavbarMenu} />
              </ul>
              <ul className="flex gap-4">
                <SocialLinks />
              </ul>
            </div>
          </div>
        </nav>
        <CollapseNavbarMenu
          refObj={collapseNavbarMenuRef}
          onCollapse={handleCollapseNavbarMenu}
        />
      </div>
    </header>
  );
};

export default Navbar;
