import { useRef } from "react";

type NavbarToggler = {
  imgCompnent: React.ReactNode;
  onCollapse: () => void;
  className: string;
};
export function NavbarToggler({
  imgCompnent,
  onCollapse,
  className,
}: NavbarToggler) {
  return (
    <button className={className} type="button" onClick={onCollapse}>
      {imgCompnent}
    </button>
  );
}
