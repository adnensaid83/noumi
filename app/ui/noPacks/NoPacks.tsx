import React, { ReactElement } from "react";
import { RegularList } from "../RegularList";
import { russoOne } from "@/app/ui/fonts";
import Image from "next/image";
import Link from "next/link";

type NoPacksProps = {
  id: string;
  title: string;
  price: string;
  coverImage: {
    src: string;
    alt: string;
  };
};

export const NoPacks = ({ packs }: { packs: NoPacksProps[] }) => {
  return (
    <section className="py-12 linear-gradient-50-50 px-12 lg:px-0">
      <h2
        className={`uppercase text-3xl md:text-4xl text-center mb-12 text-white ${russoOne.className}`}
      >
        Nos packs
      </h2>
      <div className="container mx-auto">
        <ul className="grid md:grid-cols-2 lg:grid-cols-4 gap-6 text-white">
          <RegularList
            items={packs}
            resourceName="pack"
            itemComponent={PackItem}
          />
        </ul>
      </div>
    </section>
  );
};

type PackItemProps = {
  pack: NoPacksProps;
};

function PackItem({ pack }: PackItemProps) {
  const { id, title, price, coverImage } = pack;
  return (
    <li className="relative">
      <div className="flex flex-col items-center  absolute w-full h-[65%] bottom-0">
        <div className="bg-black200 w-full mb-12 p-2 md:mb-16 md:p-4 lg:mb-20">
          <p
            className={`text-xl md:text-2xl text-center uppercase ${russoOne.className}`}
          >
            {title}
          </p>
          <p className={`md:text-xl font-light text-center`}>{price}</p>
        </div>
        <Link
          href={"/about"}
          className="border-2 border-white px-12 rounded-xl text-center transition ease-in-out duration-300 hover:bg-white hover:text-black"
        >
          Voir plus
        </Link>
      </div>
      <Image
        src={coverImage.src}
        alt={coverImage.alt}
        width={650}
        height={200}
        priority
      />
    </li>
  );
}
