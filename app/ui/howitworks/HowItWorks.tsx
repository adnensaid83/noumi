import { russoOne } from "@/app/ui/fonts";
import React, { ReactNode } from "react";
import { RegularList } from "../RegularList";
import Link from "next/link";
import Image from "next/image";

type HowItWorksProps = {
  id: string;
  title: string;
  coverImage: {
    src: string;
    alt: string;
  };
  description: string;
  path: string;
};

export const HowItWorks = ({ data }: { data: HowItWorksProps[] }) => {
  return (
    <section className="py-12 linear-gradient-50-50 lg:linear-gradient-30-30-40 px-12 lg:px-0">
      <h2
        className={`uppercase text-3xl md:text-4xl text-center mb-12 text-white ${russoOne.className}`}
      >
        Comment ça marche
      </h2>
      <div className="container mx-auto">
        <ul className="grid md:grid-cols-3 lg:grid-cols-5 gap-4 text-white">
          <RegularList
            items={data}
            resourceName="howItWork"
            itemComponent={HowItWorkItem}
          />
        </ul>
      </div>
    </section>
  );
};

function HowItWorkItem({ howItWork }: { howItWork: HowItWorksProps }) {
  const { id, title, coverImage, description } = howItWork;
  return (
    <div>
      <li className="relative w-full h-[300px] overflow-hidden ">
        <div className="flex flex-col items-center justify-center absolute w-full bottom-0 top-0 bg-black200">
          <div className=" w-full p-2">
            <p
              className={`text-xl md:text-base text-center uppercase ${russoOne.className}`}
            >
              {title}
            </p>
            <p className={`md:text-base font-light text-center`}>
              {description}
            </p>
          </div>
        </div>
        <Image
          src={coverImage.src}
          alt={coverImage.alt}
          width={650}
          height={600}
          priority
        />
      </li>
      <Link
        href={"/"}
        className="border-2 border-tertiary-light bg-tertiary-light px-12 block w-full text-center transition ease-in-out duration-300 hover:bg-transparent hover:text-tertiary-light hover:border-t-transparent"
      >
        Voir plus
      </Link>
    </div>
  );
}
