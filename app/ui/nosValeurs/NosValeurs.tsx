import { russoOne } from "@/app/ui/fonts";
import React from "react";
import { RegularList } from "../RegularList";
import Image from "next/image";

type ValeursProps = {
  id: string;
  title: string;
  description: string;
  coverImage: {
    src: string;
    alt: string;
  };
};
export const NosValeurs = ({ valeurs }: { valeurs: ValeursProps[] }) => {
  return (
    <section className="py-12 px-12 bg-white lg:px-0">
      <h2
        className={`uppercase text-3xl md:text-4xl text-center mb-12 text-black ${russoOne.className}`}
      >
        Nos <span className="text-tertiary-light">valeurs</span>
      </h2>
      <div className="container mx-auto">
        <ul className="grid place-items-center md:grid-cols-2 lg:grid-cols-4 gap-6 text-white">
          <RegularList
            items={valeurs}
            resourceName="valeur"
            itemComponent={ValeurItem}
          />
        </ul>
      </div>
    </section>
  );
};
type ValeurItemProps = {
  valeur: ValeursProps;
};
function ValeurItem({ valeur }: { valeur: any }) {
  const { title, description, coverImage } = valeur;
  return (
    <li className="text-black flex flex-col items-center gap-6 px-12 md:p-8 md:gap-8 lg:p-0">
      <Image
        src={coverImage.src}
        alt={coverImage.alt}
        className="w-16 h-12 md:w-20 md:h-16"
        width={80}
        height={80}
        priority
      />
      <p
        className={`text-xl md:text-2xl text-center uppercase  ${russoOne.className}`}
      >
        {title}
      </p>
      <p className="text-center">{description}</p>
    </li>
  );
}
